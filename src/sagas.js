import { all } from 'redux-saga/effects';
import { watchSignIn } from './Redux/Sagas/SignIn.Saga';

export default function* rootSaga() {
  yield all([watchSignIn()]);
}
