import Axios from 'axios';
import { create } from 'apisauce';
import configs from '../config';
import AsyncStorage from '@react-native-async-storage/async-storage';

const customAxiosInstance = Axios.create({
  timeout: 3 * 60 * 1000,
  baseURL: configs.apiDomain
});
customAxiosInstance.interceptors.request.use(
  async (config) => {
    const token = await AsyncStorage.getItem('token');
    config.headers.Authorization = `Bearer ${token}`;
    return config;
  },
  (error) => Promise.reject(error)
);
customAxiosInstance.interceptors.response.use(
  (response) => {
    return response;
  },
  (error) => Promise.reject(error)
);

const apisauceInstance = create({ axiosInstance: customAxiosInstance });

export const sendGet = (url, params) => apisauceInstance.get(url, { params });
export const sendPost = (url, params) => apisauceInstance.post(url, params);
export const sendPut = (url, params) => apisauceInstance.put(url, params);
export const sendPatch = (url, params) => apisauceInstance.patch(url, params);
export const sendDelete = (url, params) => apisauceInstance.delete(url, { params });
