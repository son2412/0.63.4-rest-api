import { sendPost } from './config';

export const login = ({ email, password }) => sendPost('/auth/login', { email, password });
