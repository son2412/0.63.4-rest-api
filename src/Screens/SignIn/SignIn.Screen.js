import React, { useEffect } from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  TextInput,
  StatusBar,
  Alert,
  KeyboardAvoidingView,
  TouchableWithoutFeedback,
  Keyboard,
  Platform
} from 'react-native';
import * as Animatable from 'react-native-animatable';
import Feather from 'react-native-vector-icons/Feather';
import { useNavigation } from '@react-navigation/native';
import { useDispatch, useSelector } from 'react-redux';
import { useTheme } from 'react-native-paper';
import { signInRequest } from '../../Redux/Actions/SignIn.Action';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { checkSignIn } from '../../actions';
import styles from './SignIn.Style';
import Toast from 'react-native-simple-toast';
import FontAwesome from 'react-native-vector-icons/FontAwesome';

const validateEmail = (text) => {
  var re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(text);
};
const SignInScreen = () => {
  const navigation = useNavigation();
  const dispatch = useDispatch();
  const [data, setData] = React.useState({
    email: '',
    password: '',
    check_textInputChange: false,
    secureTextEntry: true,
    isValidUser: true,
    isValidPassword: true
  });
  const signIn = useSelector((state) => state.signIn.data);
  const signInError = useSelector((state) => state.signIn.err);
  const fetching = useSelector((state) => state.signIn.fetching);
  const { colors } = useTheme();

  useEffect(() => {
    if (signIn.data && signIn.success) {
      AsyncStorage.setItem('token', signIn.data.token);
      dispatch(checkSignIn(signIn.data.token));
    }
    console.log(signIn);
    if (signInError && !signInError.success) {
      Toast.show(signInError.message);
      return;
    }
  }, [signIn, signInError, dispatch]);

  const textInputChange = (val) => {
    if (val.trim().length >= 4) {
      setData({
        ...data,
        email: val,
        check_textInputChange: true,
        isValidUser: true
      });
    } else {
      setData({
        ...data,
        email: val,
        check_textInputChange: false,
        isValidUser: false
      });
    }
  };

  const handlePasswordChange = (val) => {
    if (val.trim().length >= 5) {
      setData({ ...data, password: val, isValidPassword: true });
    } else {
      setData({ ...data, password: val, isValidPassword: false });
    }
  };

  const updateSecureTextEntry = () => setData({ ...data, secureTextEntry: !data.secureTextEntry });

  const handleValidEmail = (val) => {
    if (validateEmail(val)) {
      setData({ ...data, isValidUser: true });
    } else {
      setData({ ...data, isValidUser: false });
    }
  };

  const loginHandle = (email, password) => {
    if (data.email.length === 0 || data.password.length === 0) {
      Alert.alert('Wrong Input!', 'Phone or password field cannot be empty.', [{ text: 'Okay' }]);
      return;
    }
    dispatch(signInRequest({ email: email, password: password }));
  };

  return (
    <KeyboardAvoidingView style={styles.container} behavior={Platform.OS === 'android' && 'height'}>
      <StatusBar backgroundColor="#009387" barStyle="light-content" />
      <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
        <View style={styles.header}>
          <Text style={styles.text_header}>Biofun Việt Nam!</Text>
        </View>
      </TouchableWithoutFeedback>
      <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
        <Animatable.View animation="fadeInUpBig" style={[styles.footer, { backgroundColor: colors.background }]}>
          <Text style={[styles.text_footer, { color: colors.text }]}>Số điện thọai</Text>
          <View style={styles.action}>
            {/* <Feather name="phone" color={colors.text} size={20} /> */}
            <FontAwesome name="user-o" color={colors.text} size={20} />
            <TextInput
              placeholder="Nhập số điện thoại"
              placeholderTextColor="#666666"
              style={[styles.textInput, { color: colors.text }]}
              autoCapitalize="none"
              onChangeText={(val) => textInputChange(val)}
              onEndEditing={(e) => handleValidEmail(e.nativeEvent.text)}
            />
            {data.check_textInputChange ? (
              <Animatable.View animation="bounceIn">
                <Feather name="check-circle" color="green" size={20} />
              </Animatable.View>
            ) : null}
          </View>
          {data.isValidUser ? null : (
            <Animatable.View animation="fadeInLeft" duration={500}>
              <Text style={styles.errorMsg}>Email invalid.</Text>
            </Animatable.View>
          )}

          <Text style={[styles.text_footer, { color: colors.text, marginTop: 15 }]}>Mật khẩu</Text>
          <View style={styles.action}>
            <Feather name="lock" color={colors.text} size={20} />
            <TextInput
              placeholder="Nhập mật khẩu"
              placeholderTextColor="#666666"
              secureTextEntry={data.secureTextEntry ? true : false}
              style={[styles.textInput, { color: colors.text }]}
              autoCapitalize="none"
              onChangeText={(val) => handlePasswordChange(val)}
            />
            <TouchableOpacity onPress={updateSecureTextEntry}>
              {data.secureTextEntry ? <Feather name="eye-off" color="grey" size={20} /> : <Feather name="eye" color="grey" size={20} />}
            </TouchableOpacity>
          </View>
          {data.isValidPassword ? null : (
            <Animatable.View animation="fadeInLeft" duration={500}>
              <Text style={styles.errorMsg}>Password must be 6 characters long.</Text>
            </Animatable.View>
          )}

          <TouchableOpacity onPress={() => navigation.navigate('Forgot')}>
            <Text style={{ color: '#009387', marginTop: 15 }}>Quên mật khẩu?</Text>
          </TouchableOpacity>
          <View style={styles.button}>
            <TouchableOpacity
              style={[
                styles.signIn,
                {
                  borderColor: '#009387',
                  borderWidth: 1,
                  marginTop: 15,
                  backgroundColor: '#009387'
                }
              ]}
              onPress={() => loginHandle(data.email, data.password)}>
              <Text style={[styles.textSign, { color: '#fff' }]}>Đăng nhập</Text>
            </TouchableOpacity>
          </View>
        </Animatable.View>
      </TouchableWithoutFeedback>
    </KeyboardAvoidingView>
  );
};

export default SignInScreen;
